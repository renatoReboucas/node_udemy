// const mysql = require('../../config/mysql')
module.exports = (http) =>{
    var mysql = http.config.mysql();
    
    http.get('/', (req, res) => {
        res.render('home/index')
    });
    http.get('/noticias/form', (req, res) => {
        res.render("admin/form_add_noticia")
    });
    http.get('/noticias', (req, res) => {
        mysql.query('select * from noticias', (erro, result) => {
            res.render("noticias/noticias", {noticias: result})
            
        });  
    });
    http.get('/noticia', (req, res) => { 
        const noticiasModel = http.app.models.noticiasModel;

        noticiasModel.getNoticiasModel(mysql, (erro, result) => {
            res.render("noticias/noticia", { noticia: result });
        });
    

    });

}