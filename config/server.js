// modulo pra rodar o servidor express
const express = require('express');
const consign = require('consign');

var http = express();
http.set('view engine', 'ejs');
http.set('views', './app/views');

// lib para inportar rotas 
consign()
.include('config/mysql.js')
.then('app/routes/routes.js')
.then('app/models')
.into(http);

module.exports = http;