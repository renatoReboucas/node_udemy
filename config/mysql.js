const mysql = require('mysql');

const conMysql = () =>{
    
    // console.log("conexao db estabelecida");
    return mysql.createConnection({    
        host: 'localhost',
        port: '9000',
        user: 'root',
        password: 'root',
        database: 'noticias'
    });

}

module.exports = () =>{
    // console.log("modulo mysql carregado...");
    return conMysql;
}